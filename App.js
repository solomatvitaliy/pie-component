import React, { useState, useMemo } from 'react';
import { View, Text } from 'react-native';
import PieChart from './components/PieChart';
import styles from './App.styles';
import Labels from './components/Labels';
import getPieChartData, { mockData } from './data.js';

const App = () => {
  const [selectedSection, setSection] = useState(mockData[0]);
  const data = useMemo(() => getPieChartData(selectedSection, setSection), [selectedSection, setSection]);

  return (
    <View style={styles.container}>
      <PieChart data={data}>
        <Labels
          selectedSection={selectedSection}
        />
      </PieChart>
      <View 
        style={styles.containerAbsolute}
      >
        <Text style={styles.textGray}>{selectedSection.label}</Text>
        <Text style={styles.text}>{selectedSection.value}</Text>
      </View>
    </View>
  );
};

export default App;
