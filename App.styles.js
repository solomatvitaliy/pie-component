import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerAbsolute: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  textGray: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'gray',
  },
});

export default styles;
