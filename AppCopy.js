import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { PieChart } from 'react-native-svg-charts';
// import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
// import { G } from 'react-native-svg';

import Labels from './components/Labels';
import data from './data.js';


const App = () => {
  const [selectedSection, setSection] = useState({ id: 2, label: 'Housing', value: 10, color: '#009fe3' });

  // const data = [
  //   {
  //     key: 0,
  //     value: 5,
  //     label: 'Food & Drinks',
  //     color: '#8000ff',
  //     icon: 'food',
  //     svg: {
  //       fill: selectedSection.color && selectedSection.id === 0 ? selectedSection.color : '#8c8c8c',
  //       onPress: () => setSection({ id: data[0].key, label: data[0].label, value: data[0].value, color: data[0].color }),
  //     },
  //     arc: { padAngle: 0 },
  //   },
  //   {
  //     key: 1,
  //     value: 5,
  //     label: 'Shopping',
  //     color: '#689c00',
  //     icon: 'shopping',
  //     svg: {
  //       fill: selectedSection.color && selectedSection.id === 1 ? selectedSection.color : '#cccccc',
  //       onPress: () => setSection({ id: data[1].key, label: data[1].label, value: data[1].value, color: data[1].color }),
  //     },
  //     arc: { padAngle: 0 },
  //   },
  //   {
  //     key: 2,
  //     value: 10,
  //     label: 'Housing',
  //     color: '#009fe3',
  //     icon: 'home',
  //     svg: {
  //       fill: selectedSection.color && selectedSection.id === 2 ? selectedSection.color : '#b5b5b5',
  //       onPress: () => setSection({ id: data[2].key, label: data[2].label, value: data[2].value, color: data[2].color }),
  //     },
  //     arc: { padAngle: 0 },
  //   },
  // ];

  // const setIconColor = (index) => {
  //   console.log('index ->', index, 'id ->', selectedSection.id);
  //   if (index === selectedSection.id) {
  //     return 'white';
  //   }
  // }

  // const Labels = ({ slices }) => {
  //   return slices.map((slice, index) => {
  //     const { labelCentroid, data } = slice;
  //     return (
  //       <G
  //         key={index}
  //         x={labelCentroid[ 0 ]}
  //         y={labelCentroid[ 1 ]}
  //       >
  //         <G
  //           x={-14}
  //           y={-15}
  //         >
  //           <Icon name={data.icon} size={30} color={setIconColor(index)} />
  //         </G>
  //       </G>
  //     )
  //   });
  // };

  return (
    // <View style={styles.container}>
    <View style={{ marginTop: 300 }}>
      <PieChart
        style={{ height: 300 }}
        data={data(selectedSection, setSection)}
        innerRadius={'70%'}
      >
        <Labels
          selectedSection={selectedSection}
        />
      </PieChart>
      <View 
        style={styles.containerAbsolute}
      >
        <Text style={styles.textGray}>{selectedSection.label}</Text>
        <Text style={styles.text}>{selectedSection.value}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerAbsolute: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  textGray: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'gray',
  },
});

export default App;
