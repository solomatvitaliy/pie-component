import React from 'react';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { G } from 'react-native-svg';

const Labels = ({ slices }) => {
  return slices.map((slice, index) => {
    const { labelCentroid, data } = slice;
    return (
      <G key={index} x={labelCentroid[0]} y={labelCentroid[1]}>
        <G x={-14} y={-15}>
          <Icon 
            name={data.icon} 
            size={30} 
            color={'black'}
          />
        </G>
      </G>
    );
  });
};

export default Labels;
