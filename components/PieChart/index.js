import React from 'react';
import { PieChart as Pie } from 'react-native-svg-charts';

const PieChart = ({ data, children }) => {
  return (
    <Pie style={{ height: 300, width: 300 }} data={data} innerRadius={'70%'}>
        {children}
    </Pie>
  );
};

export default PieChart;
