const greyColors = [
    '#bababa',
    '#9e9e9e',
    '#8c8c8c',
];

export const mockData = [{
    key: 0,
    value: 5,
    label: 'Food & Drinks',
    color: '#8000ff',
    icon: 'food',
}, {
    key: 1,
    value: 5,
    label: 'Shopping',
    color: '#689c00',
    icon: 'shopping',
},
{
    key: 2,
    value: 10,
    label: 'Housing',
    color: '#009fe3',
    icon: 'home',
}];

const getPieChartData = (selectedSection, setSection) => mockData.map((item, i) => {
    item.svg = {
        fill: selectedSection.key  === item.key ? item.color : greyColors[i],
        onPress: () => {
          setSection(item)
        }
      };
    item.arc = { padAngle: 0 };
    return item;
});

export default getPieChartData;